import axios from 'axios'

// API no ar.
// Host: https://mysterious-ravine-70213.herokuapp.com

// Rotas:
//   GET /tasks
//   POST /tasks
//    {
//    task: {title: "string", 
//    date: "mm-dd-aaaa", 
//    status: ["planning", "doing", "done"]
//   }
//   GET /tasks/:id
//   PUT/PATCH /tasks/:id  
//     {
//    task: {title: "string", 
//    date: "mm-dd-aaaa", 
//    status: ["planning", "doing", "done"]
//   }
//   DELETE /tasks/:id


export const api = axios.create({
    baseURL: 'https://mysterious-ravine-70213.herokuapp.com',
})