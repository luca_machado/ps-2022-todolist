import {Button} from '../../components/button/Button'
import {ListItem} from '../../components/listItem/ListItem'
import {useEffect, useState} from 'react'
import {api} from '../../services/api'
import classes from './home.module.css'
export function Home(){
    
    // const tarefas = [{
    //     id:1,
    //     title:'passear com o cachorro',
    //     status:'concluido',
    //     date:new Date(),
        
    // },
    // {
    //     id:2,
    //     title:'Montar a aula de react',
    //     status:'concluido',
    //     date: new Date(),
    
    // },
    // {
    //     id:3,
    //     title:'Dar a aula de react',
    //     status:'pendente',
    //     date:new Date(),
        
    // }]
    const [tasks, setTasks] = useState([])  
    const [title, setTitle] = useState('')
    const [date, setDate] = useState('')
    
    function handleForm(e){
        e.preventDefault()
        let body = {
            title: title,
            status: 'done',
            date: date,
        }

        api.post('/tasks', body).then(
            (res) => {setTasks([...tasks, res.data])
            }
        ).catch( err => console.log(err))
    }

    useEffect(() => {
        getTasks()
    }, [])

    function getTasks() {
        api.get('/tasks')
        .then(res => setTasks(res.data))
        .catch(err => console.log(err))
    }




    return(
        <>
        <h1>Adicione uma tarefa</h1>
        <form action="" className={classes.form} onSubmit={handleForm}>
            <label htmlFor="title">Nome da tarefa: </label>
            <input type="text" name='title'  onChange={(e) => {setTitle(e.target.value)}}/>
            <label htmlFor="date">Prazo: </label>
            <input type="date" name='title' onChange={(e) => {setDate(e.target.value)}}/>
            <Button type={'submit'} disabled={false} content={"Adicionar tarefa +"} size="small" ></Button>
        </form>

        <h1>Lista de tarefas</h1>
        {tasks.map(tarefa =>         
        <ListItem
         title={tarefa.title}
         date={tarefa.date}
         status={tarefa.status}
         id={tarefa.id}>
        </ListItem>
)}
        </>

    )
}