import {useState} from 'react'
import classes from './ListItem.module.css'
import {Button} from '../button/Button'
import {api} from '../../services/api'

export function ListItem(props) {
   const [status, setStatus] = useState(props.status)
    function markAsDone() {
        let body = {
            title: props.title,
            status: 'done',
            date: props.date,
        }

        api.put(`/tasks/${props.id}`, body)
        .then((res) => {console.log(res)
        setStatus('done')}
        )
        .catch(err => console.log(err))
    }

    function deleteTask() {
        api.delete(`/tasks/${props.id}`)
        .then(res => console.log(res))
        .catch(err => console.log(err))
    }

    return( 
   <div className={classes.container}>
        <h2>{props.title}</h2>
        <div className={classes.information_row}>
            <div className={classes.information_text}>
                <span>Status: {status}</span>
                <span>prazo: {(props.date)}</span>      
            </div>
           { status === 'doing' ? <Button onClick={markAsDone} content={'concluir'}/> : <Button disabled={true} content={'Concluido'} />}
           <Button content={'deletar'} onClick={deleteTask} />
        </div>
    </div>
   )
}