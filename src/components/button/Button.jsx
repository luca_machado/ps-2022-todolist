import classes from './Button.module.css'

export function Button({content, onClick, type, disabled=false, color}) {
    Button.defaultProps = {
        disabled: false,
        content: 'botão',
    }
    return (
        <button disabled={disabled} type={type} onClick={onClick} className={classes.button}>{content}</button>
    )
}